public enum Tile {
	BLANK("_"),
	X("X"),
	O("O");
	
	// Fields
	private final String name;
	
	// Constructor
	private Tile(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}