import java.util.Scanner;

public class TicTacToeApp {

  public static void main(String[] args) {
    // Variables / Initialization
    Scanner reader = new Scanner(System.in);
    Board board = new Board();
    boolean gameOver = false;
    int player = 1;
    Tile playerToken = Tile.X;

    // Start the game
    System.out.println("Welcome to my game!");
    System.out.println("Player 1 = X");
    System.out.println("Player 2 = O");
    while (!gameOver) {
      System.out.println(board);

      System.out.println("It's player " + player + "'s turn!");
      // Ask the current player to input a row and column
      System.out.println("Input a row");
      int row = reader.nextInt() - 1;
      System.out.println("Input a column");
      int column = reader.nextInt() - 1;

      // Check which player's turn it is and chose player token appropriately
      if (player == 1) {
        playerToken = Tile.X;
      } else if (player == 2) {
        playerToken = Tile.O;
      }

      /*
       * If the player inputs a row or column that doesn't exist,
       * or they chose an index that is already full, ask to reinput a correct index
       */
      while (!board.placeToken(row, column, playerToken)) {
        System.out.println("Reinput a correct row");
        row = reader.nextInt();
        System.out.println("Reinput a correct column");
        column = reader.nextInt();
      }

      // Check to see if any game ending conditions are met and print results
      if (board.checkIfWinning(playerToken)) {
        System.out.println(board);
        System.out.println("Player " + player + " wins!!");
        gameOver = true;
      } else if (board.checkIfFull()) {
        System.out.println(board);
        System.out.println("It's a tie!");
        gameOver = true;
      } else {
        // Change player's turn
        if (player == 1) {
          player += 1;
        } else {
          player = 1;
        }
      }
    }

  }
}
