public class Board {

  // Fields
  private Tile[][] grid;
  final int GRID_SIZE = 3; // Modify this parameter to change grid size.

  // Constructor
  public Board() {
    grid = new Tile[GRID_SIZE][GRID_SIZE];
    for (int i = 0; i < GRID_SIZE; i++) {
      for (int j = 0; j < GRID_SIZE; j++) {
        // Initialize each index with BLANK enum.
        grid[i][j] = Tile.BLANK;
      }
    }

  }

  // Custom methods

  // The toString method prints the board in classic TicTacToe fashion.
  public String toString() {
    String boardString = "";
    for (int i = 0; i < GRID_SIZE; i++) {
      for (int j = 0; j < GRID_SIZE; j++) {
        boardString += grid[i][j].getName() + " ";
      }
      boardString += "\n";
    }
    return boardString;
  }

  // Places the player's token on the board at the row and column specified
  public boolean placeToken(int row, int col, Tile playerToken) {
    if (row > GRID_SIZE || col > GRID_SIZE) {
      return false;
    }
    if (grid[row][col] == Tile.BLANK) {
      grid[row][col] = playerToken;
      return true;
    } else {
      return false;
    }
  }

  // Checks to see if the board is completely full
  public boolean checkIfFull() {
    for (int i = 0; i < GRID_SIZE; i++) {
      for (int j = 0; j < GRID_SIZE; j++) {
        if (grid[i][j] != Tile.BLANK) {
          return false;
        }
      }
    }
    return true;
  }

  // Checks to see if a player has a column of their player token on the board
  private boolean checkIfWinningHorizontal(Tile playerToken) {
    for (int i = 0; i < GRID_SIZE; i++) {
      int counter = 0;
      for (int j = 0; j < GRID_SIZE; j++) {
        if (grid[i][j] == playerToken) {
          counter += 1;
        }
        if (counter == GRID_SIZE) {
          return true;
        }
      }
    }
    return false;
  }

  // Checks to see if a player has a row of their player token on the board
  private boolean checkIfWinningVertical(Tile playerToken) {
    for (int i = 0; i < GRID_SIZE; i++) {
      int counter = 0;
      for (int j = 0; j < GRID_SIZE; j++) {
        if (grid[j][i] == playerToken) {
          counter += 1;
        }
        if (counter == GRID_SIZE) {
          return true;
        }
      }
    }
    return false;
  }

  // Checks to see if the player has a diagonal line of their player token on the
  // board
  private boolean checkIfWinningDiagonal(Tile playerToken) {
    int counter1 = 0;
    int counter2 = 0;
    for (int i = 0; i < GRID_SIZE; i++) {
      if (grid[i][i] == playerToken) {
        counter1++;
      }
      if (grid[i][GRID_SIZE - 1 - i] == playerToken) {
        counter2++;
      }
    }
    if (counter1 == GRID_SIZE || counter2 == GRID_SIZE) {
      return true;
    }
    return false;
  }

  // Uses the previous three methods to check if the player has won the game
  public boolean checkIfWinning(Tile playerToken) {
    if (checkIfWinningHorizontal(playerToken)) {
      return true;
    } else if (checkIfWinningVertical(playerToken)) {
      return true;
    } else if (checkIfWinningDiagonal(playerToken)) {
      return true;
    } else {
      return false;
    }
  }

}
